let clipboard = "";

const sendMessageButton = document.getElementById('sendMessage')
sendMessageButton.addEventListener("click", async function(e) {
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    	chrome.tabs.sendMessage(tabs[0].id, {name: "remplis"});
	})
});
const copy = document.getElementById('COPY')
copy.addEventListener("click", async function(e) {
	navigator.clipboard.writeText(clipboard)
});


chrome.runtime.onMessage.addListener( // this is the message listener
    function(request, sender, sendResponse) {
        if (request.name === "infos") {
        	remplissage(request.message);
        }
    }
);

function remplissage(infos) {
	const container = document.getElementById("dataContainer");

	const names = ["NOM","Prix de l'action", "BVPS", "PER", "YIELD", "VE/EBITDA", "CAPI/CA", "BNA", "MARGE NETTE", "LEVIER", "ROE", "ROA", "CAPEX", "CA GROWTH"] // "EBITDA GROWTH", "DIV GROWHTH", "BNA GRWOTH"];
	let finalElements = "";

	names.forEach((name, index) => {
	  finalElements += `
	    <h2>${name}</h2>
	    <p>${infos[index]}</p>
	  `;
	  clipboard= clipboard+infos[index]+"\n"
	  console.log("copied in CP");
	});

	container.innerHTML = finalElements;
}
